import json
import logging
import sys
import time
from io import StringIO

import requests


def set_logger(
    log_name: str = "default_log",
    log_format: str = "%(asctime)s: [%(levelname)s] %(message)s",
    date_format: str = "%Y-%m-%d %H:%M:%S",
) -> tuple:
    """
    Create special purpose logger with 2 streams:
        sys.stdout (integrates with aws lambda logging)
        StringIO to enable capturing log content as a string (convenient for

    Args:
        log_name (str, optional): Optional Log name. Defaults to "default_log".
        log_format (str, optional): Format of log line. Defaults to "%(asctime)s: [%(levelname)s] %(message)s".
        date_format (str, optional): Date format. Defaults to "%Y-%m-%d %H:%M:%S".

    Returns:
        tuple: Tuple of two:
            logging.Logger,
            io.StringIO.
    """
    formatter = logging.Formatter(log_format, date_format)

    log_str = StringIO()
    log_stdout_stream = logging.StreamHandler(stream=sys.stdout)
    log_str_stream = logging.StreamHandler(stream=log_str)

    log_stdout_stream.setFormatter(formatter)
    log_str_stream.setFormatter(formatter)

    logger = logging.getLogger(log_name)
    logger.setLevel(logging.INFO)

    logger.addHandler(log_stdout_stream)
    logger.addHandler(log_str_stream)

    return logger, log_str


def slack_notify(hook_url: str, txt: str) -> int:
    """
    Send Slack notification using hook url.

    Args:
        hook_url (str): Slack hook url.
        txt (str): Notification text.

    Returns:
        int: 0 - if successfull, 1 - if notification post failed.
    """

    headers = {"Content-type": "application/json"}

    data = json.dumps(dict(text=txt, mrkdwn_in=["text", "pretext"]))

    # make exponential backoff attempts in case Slack service is unavailable
    # (it may happen with free Slack subscription)
    for n in range(0, 3):
        try:
            resp = requests.post(
                hook_url, headers=headers, data=data, verify=True, timeout=2
            )
            if resp.status_code == 200:
                return 0
            else:
                resp_txt = f"resp status {resp.status_code}: {resp.text}"
        except requests.ConnectionError as e:
            resp_txt = f"{e}"
            time.sleep(2 ** n)
            continue
        except Exception as e:
            # print to stdout/cloudwatch for future investigation
            # don't fail completely and return 1 instead
            # so can be handled by downstream
            print(f"*Slack ERROR*: {e}")
            return 1
    # exit with error if have not succeeded
    print(f"*Slack ERROR*: {resp_txt}")
    return 1
