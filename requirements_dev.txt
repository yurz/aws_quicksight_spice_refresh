black==20.8b1
boto3==1.14.45
flake8==3.8.4
jsonschema==3.2.0
pytest==6.1.2
pytest_mock==3.3.1
