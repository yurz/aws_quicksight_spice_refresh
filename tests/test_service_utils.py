import io
import logging

import pytest
import requests_mock
from service_utils import set_logger, slack_notify


def test_set_logger_return_types():
    """
    Make sure set_logger returns tuple of two correct types
    """
    result = set_logger("default_log")
    assert isinstance(result[0], logging.Logger)
    assert isinstance(result[1], io.StringIO)


def test_slack_notify_correct_hook(requests_mock):
    hook_url = "https://correct_hook_url"
    requests_mock.post(hook_url, status_code=200)
    assert slack_notify(hook_url=hook_url, txt="test") == 0


def test_slack_notify_incorrect_hook(requests_mock):
    hook_url = "https://incorrect_hook_url"
    requests_mock.post(hook_url, status_code=404)
    assert slack_notify(hook_url=hook_url, txt="test") == 1
