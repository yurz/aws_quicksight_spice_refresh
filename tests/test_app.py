import io
import logging

import pytest
from app import main, ds_refresh


not_implemented = pytest.mark.skip("test not implemented")


def test_main_exit_on_both_dashb_and_datasets_provided():
    with pytest.raises(SystemExit):
        main(event={"dashb_id": "1111-2222", "dataset_ids": "1111,2222"})


def test_main_exit_on_neither_dashb_or_datasets_provided():
    with pytest.raises(SystemExit):
        main(event={"dashb_id": "", "dataset_ids": ""})


@not_implemented
def test_ds_refresh_return_schema_if_success():
    pass


@not_implemented
def test_ds_refresh_return_schema_if_failed():
    pass


@not_implemented
def test_ds_refresh_skip_if_not_spice():
    pass
